const chai = require('chai');
const{ assert }= require('chai');
const http = require('chai-http');
chai.use(http);

describe('forex_api_test_suite', () => {
	
	it('test_api_get_rates_is_running', (done) => {
		chai.request('http://localhost:5001').get('/forex/getRates')
		.end((err, res) => {
			assert.isDefined(res);
			done();
		})
	})
	
	it('test_api_get_rates_returns_200', (done) => {
		chai.request('http://localhost:5001')
		.get('/forex/rates')
		.end((err, res) => {
			assert.equal(res.status,200)
			done();	
		})		
	})
	

})
//Minimum Requirements

describe('forex_api_test_suite_currency', () => {

// 1. Check if post/currency is running.
	it('test_api_post_currency_is_running', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.end((err, res) => {
			assert.isDefined(res);
			done();
		})
	})

// 2. Check if post/currency returns status 400 if name is missing.
	it('test_api_post_currency_returns_400_if_no_name', (done) => {		
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({

			alias: "Teyvat Currency",
			ex : {
				'mora': 100,
				'primo': 160,
				'genesis crystal': 320	
			  }
	      
		})
		.end((err, res) => {
			assert.equal(res.status,400);
			done();	
		})		
	})
// 3. Check if post/currency returns status 400 if name is not a string.
	it('test_api_post_currency_returns_400_if_name_not_a_string', (done) => {		
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			name: 160,
			alias: "Teyvat Currency",
			ex : {
				'mora': 100,
				'primo': 160,
				'genesis crystal': 320	
			  }
		
		})
		.end((err, res) => {
			assert.equal(res.status,400);
			done();	
		})		
	})
// 4 Check if post/currency returns status 400 if name is empty.	
	it('test_api_post_currency_returns_400_if_name_is_empty', (done) => {		
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			name: null,
			alias: "Teyvat Currency",
			ex : {
				'mora': 100,
				'primo': 160,
				'genesis crystal': 320	
			  }
		
		})
		.end((err, res) => {
			assert.equal(res.status,400);
			done();	
		})		
	})
// 5. Check if post/currency returns status 400 if ex is missing

	it('test_api_post_currency_returns_400_if_no_ex', (done) => {		
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
		    name: "mora",
			alias: "Teyvat Currency"
	      
		})
		.end((err, res) => {
			assert.equal(res.status,400);
			done();	
		})		
	})
// 6. Check if post/currency returns status 400 if ex is not an object.
	it('test_api_post_currency_returns_400_if_not_an_object', (done) => {		
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: 'php',
			name: 'Philippine Peso',
			ex: "usd: 0.020"

			
		
		})
		.end((err, res) => {
			assert.equal(res.status,400);
			done();	
		})		
	})
// 7. Check if post/currency returns status 400 if ex is empty.	
	it('test_api_post_currency_returns_400_if_ex_is_empty', (done) => {		
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: 'php',
			name: 'Philippine Peso',
			ex: {}
		})
		.end((err, res) => {
			assert.equal(res.status,400);
			done();	
		})		
	})

// 8. Check if post/currency returns status 400 if alias is missing.

	it('test_api_post_currency_returns_400_if_no_alias', (done) => {		
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({

			name: 'Japanese Yen',
			ex: {
			  'peso': 0.47,
			  'usd': 0.0092,
			  'won': 10.93,
			  'yuan': 0.065
			}
	      
		})
		.end((err, res) => {
			assert.equal(res.status,400);
			done();	
		})		
	})
// 9. Check if post/currency returns status 400 if alias is not a string.
	it('test_api_post_currency_returns_400_if_not_a_string', (done) => {		
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: 'yen',
			name: 10000,
			ex: {
			  'peso': 0.47,
			  'usd': 0.0092,
			  'won': 10.93,
			  'yuan': 0.065
			}
		
		})
		.end((err, res) => {
			assert.equal(res.status,400);
			done();	
		})		
	})
// 10. Check if post/currency returns status 400 if alias is empty.
	it('test_api_post_currency_returns_400_if_alias_is_empty', (done) => {		
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: null,
			name: 'Japanese Yen',
			ex: {
			  'peso': 0.47,
			  'usd': 0.0092,
			  'won': 10.93,
			  'yuan': 0.065
			}
		
		})
		.end((err, res) => {
			assert.equal(res.status,400);
			done();	
		})		
	})

/// 11. Check if post/currency returns status 400 if all fields are complete but there is a duplicate alias.
	it('test_api_post_currency_returns_400_if_all_fields_are_complete_but_there_is_a_duplicate_alias', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: 'yuan',
      		name: 'Chinese Yuan'
			
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})

// 12. Check if post/currency returns status 200 if all fields are complete and there are no duplicates.

	it('test_api_post_currency_returns_200_if_complete_input_given', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: 'usd',
			name: 'United States Dollar',
			ex: {
				'peso': 50.73,
				'won': 1187.24,
				'yen': 108.63,
				'yuan': 7.03
			}
		})
		.end((err, res) => {
			assert.equal(res.status,200)
			done();
		})
	}) 

})

