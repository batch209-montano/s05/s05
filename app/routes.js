const { exchangeRates } = require('../src/util.js');
const express = require("express");
const router = express.Router();

const app = express();
app.use(express.json());

app.get('/rates', (req, res) => {
	return res.send(exchangeRates);
});

	router.get('/rates', (req, res) => {
		return res.status(200).send(exchangeRates);
	})

app.post('/currency', (req, res) => {
	return res.send(currency);
});

	router.post('/currency', (req, res) => {

		let forex = exchangeRates.find((exRates) => {

			return exRates.alias === req.body.alias
	
		});

// 2. Check if post/currency returns status 400 if name is missing.
		if(!req.body.hasOwnProperty('name')){
		
			return res.status(400).send({
				'error' : 'Bad Request: missing required parameter NAME'
			})

		}

// 3. Check if post/currency returns status 400 if name is not a string.
		if(typeof req.body.name !== 'string'){
			return res.status(400).send({
				'error' : 'Bad Request: Name has to be string'
			})
		}

// 4 Check if post/currency returns status 400 if name is empty.
		if(typeof req.body.name === null){
			return res.status(400).send({
				'error' : 'Bad Request: Name is empty'
			})
		}	
		
// 5. Check if post/currency returns status 400 if ex is missing
		if(!req.body.hasOwnProperty('ex')){
			return res.status(400).send({
				'error' : 'Bad Request: missing required parameter EX'
			})

		}

// 6. Check if post/currency returns status 400 if ex is not an object.
		if(typeof req.body.ex !== 'object'){
			return res.status(400).send({
				'error' : 'Bad Request: Ex has to be an object'
			})
		}

// 7. Check if post/currency returns status 400 if ex is empty.	

		if(Object.keys(req.body.ex).length === 0 && req.body.ex.constructor === Object){
			return res.status(400).send({
				'error' : 'Bad Request: Ex is empty.'
			})
		}

// 8. Check if post/currency returns status 400 if alias is missing.
		if(!req.body.hasOwnProperty('alias')){
			return res.status(400).send({
				'error' : 'Bad Request: missing required parameter ALIAS'
			})

		}

// 9. Check if post/currency returns status 400 if alias is not a string.
		if(typeof req.body.alias !== 'string'){
			return res.status(400).send({
				'error' : 'Bad Request: Alias has to be a string'
			})
		}

// 10. Check if post/currency returns status 400 if alias is empty.
		if(typeof req.body.ex === null){
					return res.status(400).send({
						'error' : 'Bad Request: alias is empty.'
					})
		}

// 12. Check if post/currency returns status 200 if all fields are complete and there are no duplicates.
		if(forex !== req.body){
				return res.status(200).send({
					'success': 'Congratulations! No duplicates.'
				})
			}
	

// 11. Check if post/currency returns status 400 if all fields are complete but there is a duplicate alias.
		if(forex.alias === req.body.alias ){
			return res.status(400).send({
				'error' : 'Bad Request: Alias is already exists.'
			})
		}

	});		

module.exports = router;
